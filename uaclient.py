#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""

import socket
import sys
import hashlib
import secrets
import simplertp
import time
import random
import os
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class LogWriter:
    """Clase para escribir en el fichero de log."""

    def __init__(self, filename):
        """Constructor para iniciar las variables."""
        self.filename = filename
        self.methods = {
            'Received from': self.received_from,
            'Error': self.error,
            'Sent to': self.sent_to,
            'Starting': self.starting,
            'Finishing': self.finishing
        }

    def received_from(self, ip, port, text):
        """Funcion para escribir mensajes de tipo Received from."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        address = ip + ':' + str(port)
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Received from ' + address + ': ' + text
        self.write(line)

    def error(self, text):
        """Funcion para escribir mensajes de error."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Error ' + ': ' + text
        self.write(line)

    def sent_to(self, ip, port, text):
        """Funcion para escribir mensajes de tipo Sent to."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        address = ip + ':' + str(port)
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Sent to ' + address + ': ' + text
        self.write(line)

    def starting(self):
        """Funcion para escribir mensajes de inicio."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Starting... '
        self.write(line)

    def finishing(self):
        """Funcion para escribir mensajes de final."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Finishing... '
        self.write(line)

    def write(self, text):
        """Funcion para escribir en el fichero."""
        with open(self.filename, 'a') as logfile:
            logfile.write(text.replace('\r\n', ' ') + '\n')


class XMLReader(ContentHandler):
    """Clase con la que podemos usar SMIL."""

    def __init__(self):
        """Constructor para iniciar las variables."""
        self.dicc = {}
        self.att = {'account': ['username', 'passwd'],
                    'uaserver': ['ip', 'puerto'],
                    'rtpaudio': ['puerto'],
                    'regproxy': ['ip', 'puerto'],
                    'log': ['path'],
                    'audio': ['path']}

    def startElement(self, name, attrs):
        """Metodo al que se llama cuando se abre una etiqueta."""
        diccionario = {}
        if name in self.att:
            for att in self.att[name]:
                diccionario[att] = attrs.get(att, '')
                if diccionario[att] == '' and att == 'ip':
                    if att == 'ip':
                        diccionario[att] = '127.0.0.1'
            self.dicc[name] = diccionario

    def get_tags(self):
        """Devuelve la lista con los atributos."""
        return self.dicc


class SIPMethod:
    """Clase para devolver los mensajes SIP."""

    def __init__(self, tags):
        """Funcion inicializadora de la clase SIPMethod."""
        self.tags = tags
        self.methods = {'register': self.register,
                        'invite': self.invite,
                        'bye': self.bye,
                        'ack': self.ack}

    def register(self, opcion, nonce=''):
        """Funcion para devolver los mensajes de tipo Register."""
        line = 'REGISTER sip:' + self.tags['account']['username'] + ':'
        line += self.tags['uaserver']['puerto'] + ' SIP/2.0\r\nExpires: '
        line += opcion
        if nonce != '':
            response = self.digest_response(nonce)
            line += '\r\nAuthorization: Digest response="' + response + '"'
        return line + '\r\n\r\n'

    def digest_response(self, nonce):
        """Funcion para generar el digest response."""
        digest = hashlib.blake2b()
        response = ''
        for c in nonce:
            y = 0
            for p in self.tags['account']['passwd']:
                y += (ord(c) + ord(p))/2
            response += chr(int(y/len(self.tags['account']['passwd'])))
        digest.update(bytes(response, 'utf-8'))
        digest.digest()
        return digest.hexdigest()

    def sdp_body(self):
        """Funcion para obtener el cuerpo SDP de un mensaje."""
        body = 'v=0\r\no=' + self.tags['account']['username'] + ' '
        body += self.tags['uaserver']['ip'] + '\r\ns=macarrones\r\nt=0\r\n'
        body += 'm=audio ' + self.tags['rtpaudio']['puerto'] + ' RTP'
        return body

    def invite(self, opcion):
        """Funcion para devolver los mensajes de tipo Invite."""
        body = self.sdp_body()
        line = 'INVITE sip:' + opcion + ' SIP/2.0\r\nContent-Type: '
        line += 'application/sdp\r\nContent-Length: ' + str(len(body))
        line += '\r\n\r\n' + body
        return line + '\r\n\r\n'

    def bye(self, opcion):
        """Funcion para devolver los mensajes de tipo Bye."""
        return 'BYE sip:' + opcion + ' SIP/2.0\r\n\r\n'

    def ack(self, opcion):
        """Funcion para devolver los mensajes de tipo Ack."""
        return 'ACK sip:' + opcion + ' SIP/2.0\r\n\r\n'


if __name__ == '__main__':
    try:
        config = sys.argv[1]
        metodo = sys.argv[2].lower()
        opcion = sys.argv[3]
        parser = make_parser()
        cHandler = XMLReader()
        parser.setContentHandler(cHandler)
        parser.parse(open(config))
        tags = cHandler.get_tags()
        pr = (tags['regproxy']['ip'], int(tags['regproxy']['puerto']))
        messages = SIPMethod(tags)
        log = LogWriter(tags['log']['path'])
        log.methods['Starting']()
        LINE = messages.methods[metodo](opcion)
    except (IndexError, ValueError):
        sys.exit("Usage: python3 uaclient.py config metodo opcion")

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect(pr)

        my_socket.send(bytes(LINE, 'utf-8'))
        log.methods['Sent to'](tags['regproxy']['ip'],
                               tags['regproxy']['puerto'],
                               LINE)
        data = my_socket.recv(1024).decode("utf-8")
        log.methods['Received from'](tags['regproxy']['ip'],
                                     tags['regproxy']['puerto'],
                                     data)
        print(data)
        if metodo == 'register' and '401 Unauthorized' in data:
            nonce = data.split('"')[1]
            LINE = messages.methods[metodo](opcion, nonce)
            my_socket.send(bytes(LINE, 'utf-8'))
            log.methods['Sent to'](tags['regproxy']['ip'],
                                   tags['regproxy']['puerto'],
                                   LINE)
            data = my_socket.recv(1024).decode("utf-8")
            log.methods['Received from'](tags['regproxy']['ip'],
                                         tags['regproxy']['puerto'],
                                         data)
            print(data)
        if '100 Trying' in data and '180 Ringing' in data and '200 OK' in data:
            my_socket.send(bytes(messages.methods['ack'](opcion), 'utf-8'))
            log.methods['Sent to'](tags['regproxy']['ip'],
                                   tags['regproxy']['puerto'],
                                   LINE)
            ip = data.split('\r\n')[15].split()[1]
            puerto = int(data.split('\r\n')[18].split()[1])
            BIT = secrets.randbelow(1)
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(version=2, marker=BIT,
                                  payload_type=14, ssrc=500005)
            csrc = []
            for i in range(5):
                csrc.append(random.randint(1, 9))
            RTP_header.setCSRC(csrc)
            audio = simplertp.RtpPayloadMp3(tags['audio']['path'])
            simplertp.send_rtp_packet(RTP_header, audio,
                                      ip, puerto)
            cvlc = 'cvlc rtp://@' + ip + ':' + str(puerto)
            os.system(cvlc)

        print("Terminando socket...")

    print("Fin.")
