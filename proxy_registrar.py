#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import socket
import sys
import simplertp
import secrets
import hashlib
import json
import time
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class LogWriter:
    """Clase para escribir en el fichero de log."""

    def __init__(self, filename):
        """Constructor para iniciar las variables."""
        self.filename = filename
        self.methods = {
            'Received from': self.received_from,
            'Error': self.error,
            'Sent to': self.sent_to,
            'Starting': self.starting,
            'Finishing': self.finishing
        }

    def received_from(self, ip, port, text):
        """Funcion para escribir mensajes de tipo Received from."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        address = ip + ':' + str(port)
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Received from ' + address + ': ' + text
        self.write(line)

    def error(self, text):
        """Funcion para escribir mensajes de error."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Error ' + ': ' + text
        self.write(line)

    def sent_to(self, ip, port, text):
        """Funcion para escribir mensajes de tipo Sent to."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        address = ip + ':' + str(port)
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Sent to ' + address + ': ' + text
        self.write(line)

    def starting(self):
        """Funcion para escribir mensajes de inicio."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Starting... '
        self.write(line)

    def finishing(self):
        """Funcion para escribir mensajes de final."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Finishing... '
        self.write(line)

    def write(self, text):
        """Funcion para escribir en el fichero."""
        with open(self.filename, 'a') as logfile:
            logfile.write(text.replace('\r\n', ' ') + '\n')


class XMLReader(ContentHandler):
    """Clase con la que podemos usar SMIL."""

    def __init__(self):
        """Constructor para iniciar las variables."""
        self.dicc = {}
        self.att = {'server': ['name', 'ip', 'puerto'],
                    'database': ['path', 'passwdpath'],
                    'log': ['path']}

    def startElement(self, name, attrs):
        """Metodo al que se llama cuando se abre una etiqueta."""
        diccionario = {}
        if name in self.att:
            for att in self.att[name]:
                diccionario[att] = attrs.get(att, '')
                if diccionario[att] == '' and (att == 'name' or att == 'ip'):
                    if att == 'name':
                        diccionario[att] = 'Manucho'
                    if att == 'ip':
                        diccionario[att] = '127.0.0.1'
            self.dicc[name] = diccionario

    def get_tags(self):
        """Devuelve la lista con los atributos."""
        return self.dicc


class SIPHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    diccreg = {}
    dicccon = {}

    def register2json(self):
        """Funcion para guardar en formato json la base de datos."""
        filejson = tags['database']['path']
        with open(filejson, 'w') as jsonfile:
            json.dump(self.diccreg, jsonfile, indent=4)

    def json2register(self):
        """Funcion para obtener la base de datos."""
        try:
            with open(tags['database']['path'], 'r') as jsonfile:
                self.diccreg = json.load(jsonfile)
        except FileNotFoundError:
            pass

    def password2json(self):
        """Funcion para guardar en formato json las contraseñas."""
        filejson = tags['database']['passwdpath']
        with open(filejson, 'w') as jsonfile:
            json.dump(self.dicccon, jsonfile, indent=4)

    def json2password(self):
        """Funcion para obtener las contraseñas."""
        try:
            with open(tags['database']['passwdpath'], 'r') as jsonfile:
                self.dicccon = json.load(jsonfile)
        except FileNotFoundError:
            pass

    def header(self, data, user_destino):
        """Funcion para añadir la cabecera del proxy."""
        h = 'Via: SIP/2.0/UDP ' + tags['server']['ip'] + ':'
        h += tags['server']['puerto'] + '\r\nTo: sip:' + user_destino
        msg = ''
        for line in data.split('\r\n'):
            if 'SIP/2.0' in line:
                msg += line + '\r\n' + h + '\r\n'
            else:
                msg += line + '\r\n'
        return msg

    def delete_client(self):
        """Funcion para borrar los clientes que hayan caducado."""
        aux = self.diccreg.copy()
        now = time.time() + 3600
        for user in aux:
            tm = self.diccreg[user]['fecha'] + self.diccreg[user]['expires']
            if now >= tm:
                del self.diccreg[user]

    def digest_response(self, data):
        """Funcion para generar el digest response."""
        digest = hashlib.blake2b()
        response = ''
        nonce = data['nonce']
        password = data['passwd']
        for c in nonce:
            y = 0
            for p in password:
                y += (ord(c) + ord(p))/2
            response += chr(int(y/len(password)))
        digest.update(bytes(response, 'utf-8'))
        digest.digest()
        return digest.hexdigest()

    def receive_register(self, data):
        """Funcion para recibir los mensajes de tipo Register."""
        username = data.split()[1].split(':')[1]
        if username in self.diccreg:
            expires = int(data.split('\r\n')[1].split()[1])
            if expires != 0:
                now = time.time() + 3600
                self.diccreg[username]['fecha'] = now
                self.diccreg[username]['expires'] = expires
            else:
                del self.diccreg[username]
            self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
            log.methods['Sent to'](self.client_address[0],
                                   self.client_address[1],
                                   'SIP/2.0 200 OK\r\n\r\n')
        else:
            if 'Authorization: Digest response' in data:
                resp_user = data.split('"')[1]
                response = self.digest_response(self.dicccon[username])
                if secrets.compare_digest(resp_user, response):
                    puerto = int(data.split()[1].split(':')[2])
                    ip = self.client_address[0]
                    expires = int(data.split('\r\n')[1].split()[1])
                    direccion = ip + ':' + str(puerto)
                    now = time.time() + 3600
                    self.diccreg[username] = {'puerto': puerto,
                                              'ip': ip,
                                              'direccion': direccion,
                                              'fecha': now,
                                              'expires': expires}
                    self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
                    log.methods['Sent to'](self.client_address[0],
                                           self.client_address[1],
                                           'SIP/2.0 200 OK\r\n\r\n')
                else:
                    self.wfile.write(b'SIP/2.0 400 Bad Request\r\n\r\n')
                    log.methods['Sent to'](self.client_address[0],
                                           self.client_address[1],
                                           'SIP/2.0 400 Bad Request\r\n\r\n')
                self.dicccon[username]['nonce'] = ''

            else:
                expires = int(data.split('\r\n')[1].split()[1])
                if expires != 0:
                    LINE = 'SIP/2.0 401 Unauthorized\r\n'
                    LINE += 'WWW Authenticate: Digest nonce="'
                    nonce = secrets.token_hex(4).upper()
                    LINE += nonce + '"\r\n\r\n'
                    self.dicccon[username]['nonce'] = nonce
                    self.wfile.write(bytes(LINE, 'utf-8'))
                    log.methods['Sent to'](self.client_address[0],
                                           self.client_address[1],
                                           LINE)
                else:
                    self.wfile.write(b'SIP/2.0 404 User Not Found\r\n\r\n')
                    log.methods['Sent to'](self.client_address[0],
                                           self.client_address[1],
                                           'SIP/2.0 404 User Not Found')

    def reenviar(self, data, ip, puerto):
        """Funcion utilizada para reenviar mensajes."""
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            my_socket.connect((ip, puerto))

            my_socket.send(bytes(data, 'utf-8'))
            log.methods['Sent to'](ip,
                                   puerto,
                                   data)
            resp = my_socket.recv(1024)
            log.methods['Received from'](ip,
                                         puerto,
                                         resp.decode('utf-8'))
        return resp

    def receive_invite(self, data):
        """Funcion utilizada para recibir mensajes de tipo Invite."""
        user_origen = data.split('\r\n')[5].split('=')[1].split()[0]
        user_destino = data.split()[1].split(':')[1]
        if user_origen in self.diccreg and user_destino in self.diccreg:
            ip_destino = self.diccreg[user_destino]['ip']
            puerto_destino = self.diccreg[user_destino]['puerto']
            r = self.header(data, user_destino)
            resp = self.reenviar(r, ip_destino, puerto_destino)
            r = self.header(resp.decode('utf-8'), user_destino)
            self.wfile.write(bytes(r, 'utf-8'))
            log.methods['Sent to'](self.client_address[0],
                                   self.client_address[1],
                                   r)
        else:
            self.wfile.write(b'SIP/2.0 404 User Not Found\r\n\r\n')
            log.methods['Sent to'](self.client_address[0],
                                   self.client_address[1],
                                   'SIP/2.0 404 User Not Found\r\n\r\n')

    def handle(self):
        """Funcion Handle."""
        self.json2register()
        self.json2password()
        self.delete_client()
        data = self.rfile.read().decode("utf-8")
        log.methods['Received from'](self.client_address[0],
                                     self.client_address[1],
                                     data)
        print(data)
        metodo = data.split()[0]
        if metodo == 'REGISTER':
            self.receive_register(data)
        if metodo == 'INVITE':
            self.receive_invite(data)
        if metodo in ['ACK', 'BYE']:
            username = data.split()[1].split(':')[1]
            if username in self.diccreg:
                ip = self.diccreg[username]['ip']
                puerto = self.diccreg[username]['puerto']
                r = self.header(data, username)
                resp = self.reenviar(r, ip, puerto)
                r = self.header(resp.decode('utf-8'), username)
                self.wfile.write(bytes(r, 'utf-8'))
                log.methods['Sent to'](self.client_address[0],
                                       self.client_address[1],
                                       r)
            else:
                self.wfile.write(b'SIP/2.0 404 User Not Found\r\n\r\n')
                log.methods['Sent to'](self.client_address[0],
                                       self.client_address[1],
                                       'SIP/2.0 404 User Not Found\r\n\r\n')
        self.register2json()
        self.password2json()


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    try:
        config = sys.argv[1]
        parser = make_parser()
        cHandler = XMLReader()
        parser.setContentHandler(cHandler)
        parser.parse(open(config))
        tags = cHandler.get_tags()
        log = LogWriter(tags['log']['path'])
        log.methods['Starting']()
    except(IndexError, ValueError):
        sys.exit("Usage: python3 proxy_registrar.py config")

    serv = socketserver.UDPServer((tags['server']['ip'],
                                   int(tags['server']['puerto'])), SIPHandler)
    print('Server', tags['server']['name'], 'listening at port',
          tags['server']['puerto'], '...')
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        sys.exit("Finalizando servidor")
