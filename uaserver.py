#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import simplertp
import secrets
import random
import time
import os
from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class LogWriter:
    """Clase para escribir en el fichero de log."""

    def __init__(self, filename):
        """Constructor para iniciar las variables."""
        self.filename = filename
        self.methods = {
            'Received from': self.received_from,
            'Error': self.error,
            'Sent to': self.sent_to,
            'Starting': self.starting,
            'Finishing': self.finishing
        }

    def received_from(self, ip, port, text):
        """Funcion para escribir mensajes de tipo Received from."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        address = ip + ':' + str(port)
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Received from ' + address + ': ' + text
        self.write(line)

    def error(self, text):
        """Funcion para escribir mensajes de error."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Error ' + ': ' + text
        self.write(line)

    def sent_to(self, ip, port, text):
        """Funcion para escribir mensajes de tipo Sent to."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        address = ip + ':' + str(port)
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Sent to ' + address + ': ' + text
        self.write(line)

    def starting(self):
        """Funcion para escribir mensajes de inicio."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Starting... '
        self.write(line)

    def finishing(self):
        """Funcion para escribir mensajes de final."""
        h = int(time.strftime('%H', time.gmtime(time.time() + 3600))) * 3600
        m = int(time.strftime('%M', time.gmtime(time.time() + 3600))) * 60
        s = int(time.strftime('%S', time.gmtime(time.time() + 3600)))
        t = h + m + s
        now = time.strftime('%Y-%m-%d', time.gmtime(time.time() + 3600))
        line = now + ' ' + str(t) + ' Finishing... '
        self.write(line)

    def write(self, text):
        """Funcion para escribir en el fichero."""
        with open(self.filename, 'a') as logfile:
            logfile.write(text.replace('\r\n', ' ') + '\n')


class XMLReader(ContentHandler):
    """Clase con la que podemos usar SMIL."""

    def __init__(self):
        """Constructor para iniciar las variables."""
        self.dicc = {}
        self.att = {'account': ['username', 'passwd'],
                    'uaserver': ['ip', 'puerto'],
                    'rtpaudio': ['puerto'],
                    'regproxy': ['ip', 'puerto'],
                    'log': ['path'],
                    'audio': ['path']}

    def startElement(self, name, attrs):
        """Metodo al que se llama cuando se abre una etiqueta."""
        diccionario = {}
        if name in self.att:
            for att in self.att[name]:
                diccionario[att] = attrs.get(att, '')
                if diccionario[att] == '' and att == 'ip':
                    if att == 'ip':
                        diccionario[att] = '127.0.0.1'
            self.dicc[name] = diccionario

    def get_tags(self):
        """Devuelve la lista con los atributos."""
        return self.dicc


class RTPHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    rtp = []

    def sdp_body(self):
        """Funcion para obtener el cuerpo SDP de un mensaje."""
        body = 'v=0\r\no=' + tags['account']['username'] + ' '
        body += tags['uaserver']['ip'] + '\r\ns=macarrones\r\nt=0\r\nm=audio '
        body += tags['rtpaudio']['puerto'] + ' RTP'
        return body

    def receive_invite(self, data):
        """Funcion para recibir mensajes Invite."""
        self.rtp.append(data.split('\r\n')[7].split()[1])
        self.rtp.append(int(data.split('\r\n')[10].split()[1]))
        mess = 'SIP/2.0 100 Trying\r\n\r\n'
        mess += 'SIP/2.0 180 Ringing\r\n\r\nSIP/2.0 200 OK\r\n'
        sdp = self.sdp_body()
        mess += 'Content-Type: application/sdp\r\nContent-Length: '
        mess += str(len(sdp)) + '\r\n\r\n' + sdp + '\r\n\r\n'
        self.wfile.write(bytes(mess, 'utf-8'))
        log.methods['Sent to'](self.client_address[0],
                               self.client_address[1],
                               mess)

    def receive_ack(self, data):
        """Funcion para recibir mensajes Ack."""
        if self.rtp != []:
            BIT = secrets.randbelow(1)
            RTP_header = simplertp.RtpHeader()
            RTP_header.set_header(version=2, marker=BIT,
                                  payload_type=14, ssrc=200002)
            csrc = []
            for i in range(5):
                csrc.append(random.randint(1, 9))
            RTP_header.setCSRC(csrc)
            audio = simplertp.RtpPayloadMp3(tags['audio']['path'])
            simplertp.send_rtp_packet(RTP_header, audio,
                                      self.rtp[0], self.rtp[1])
            cvlc = 'cvlc rtp://@' + self.rtp[0] + ':' + str(self.rtp[1])
            os.system(cvlc)
            self.rtp = []

    def receive_bye(self, data):
        """Funcion para recibir mensajes Bye."""
        self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
        log.methods['Sent to'](self.client_address[0],
                               self.client_address[1],
                               'SIP/2.0 200 OK\r\n\r\n')

    def handle(self):
        """Funcion Handle."""
        data = self.rfile.read().decode('utf-8')
        log.methods['Received from'](self.client_address[0],
                                     self.client_address[1],
                                     data)
        print(data)
        metodo = data.split()[0]
        if metodo in ['INVITE', 'ACK', 'BYE']:
            if data.split()[2] != 'SIP/2.0':
                self.wfile.write(b'SIP/2.0 400 Bad Request\r\n\r\n')
                log.methods['Sent to'](self.client_address[0],
                                       self.client_address[1],
                                       'SIP/2.0 400 Bad Request\r\n\r\n')
            else:
                if metodo == 'INVITE':
                    self.receive_invite(data)
                if metodo == 'ACK':
                    self.receive_ack(data)
                if metodo == 'BYE':
                    self.receive_bye(data)
        else:
            self.wfile.write(b'SIP/2.0 405 Method Not Allowed\r\n\r\n')
            log.methods['Sent to'](self.client_address[0],
                                   self.client_address[1],
                                   'SIP/2.0 405 Method Not Allowed\r\n\r\n')


if __name__ == '__main__':
    # Creamos servidor de eco y escuchamos
    try:
        config = sys.argv[1]
        parser = make_parser()
        cHandler = XMLReader()
        parser.setContentHandler(cHandler)
        parser.parse(open(config))
        tags = cHandler.get_tags()
        log = LogWriter(tags['log']['path'])
        log.methods['Starting']()
        address = (tags['uaserver']['ip'], int(tags['uaserver']['puerto']))
    except(IndexError, ValueError):
        sys.exit('Usage: python3 uaserver.py config')

    serv = socketserver.UDPServer(address, RTPHandler)
    print('Listening...')
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        sys.exit('Finalizando servidor')
